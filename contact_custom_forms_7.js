Drupal.behaviors.contact_custom = {
    attach: function(context) {
        jQuery('#contact-site-form #edit-cid', context).change(function() {
            window.location.href = Drupal.settings.contact_forms[ parseInt( jQuery(this).val() ) ];
        })
    }
}